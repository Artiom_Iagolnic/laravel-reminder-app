<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;

class LoginController extends Controller
{
    public function index(): View
    {
        return view("login");
    }

    public function login(Request $request): RedirectResponse
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route('appointments.index')->with('success', 'login successful');
        }

        return back()->withErrors(['email' => 'Invalid email or password'])->withInput();
    }
    public function logout(): RedirectResponse
    {
        Auth::logout();
        return redirect()->route('home')->with('success', 'logout successful');
    }
}
