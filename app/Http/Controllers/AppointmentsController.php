<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\View\View;
use App\Mail\ReminderEmail;
use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Jobs\DeleteAppointmentJob;
use App\Jobs\SendReminderEmailJob;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewAppointmentInfoEmail;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Queue;

class AppointmentsController extends Controller
{
    public function index(): View
    {
        $user = auth()->user();
        $appointments = $user->appointments;

        return view('appointments', compact('user', 'appointments'));
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'title' => 'required|string',
            'description' => 'string',
            'start-time' => 'required|date',
            'end-time' => 'required|date|after:start-time',
            'reminder' => 'required|integer'
        ]);

        $user = auth()->user();

        $appointment = $user->appointments()->create([
            'title' => $request->title,
            'description' => $request->description,
            'start_time' => ($request->input('start-time')),
            'end_time' => ($request->input('end-time')),
        ]);


        $reminder = $appointment->reminder()->create([
            'days_before' => $request->input('reminder'),
        ]);

        $reminderDateTime = $appointment->start_time->subDays($reminder->days_before);

        Mail::to($user->email)->send(new NewAppointmentInfoEmail($appointment->title, $user->name, $reminderDateTime));

        Mail::to($user->email)->later($reminderDateTime, new ReminderEmail($appointment->title, $reminder->days_before));

        return redirect()->route('appointments.index')->with('success', 'Appointment added successfully');
    }

    public function destroy(string $id): RedirectResponse
    {
        $appointment = Appointment::findOrFail($id);

        if ($appointment->reminder) {
            $appointment->reminder->delete();
        }

        $appointment->delete();

        return back()->with('success', 'deleted successfully');
    }

    public function show(string $id): View
    {
        return view('show-appointment', [
            'appointment' => Appointment::findOrFail($id)
        ]);
    }

    public function update(Request $request, string $id): RedirectResponse
    {
        $appointment = Appointment::findOrFail($id);

        $request->validate([
            'title' => 'required|string',
            'description' => 'string',
            'start-time' => 'required|date',
            'end-time' => 'required|date|after:start-time',
            'reminder' => 'required|integer'
        ]);

        $appointment->update([
            'title' => $request->title,
            'description' => $request->description,
            'start_time' => ($request->input('start-time')),
            'end_time' => ($request->input('end-time')),
        ]);

        $appointment->reminder->update([
            'days_before' => $request->input('reminder'),
        ]);

        return redirect()->route('appointments.index')->with('success', 'appointment updated successfully');
    }
}
