**Prerequisites**

PHP (>= 7.x)
Composer
MySQL or PostgreSQL (or another database of your choice)

**Getting Started**

Install dependencies:
composer install

Copy the environment file:
cp .env.example .env

Generate an application key:
php artisan key:generate

Configure the database:
Open the .env file and set the database connection details:
DB_CONNECTION=mysql
DB_HOST=your_database_host
DB_PORT=your_database_port
DB_DATABASE=your_database_name
DB_USERNAME=your_database_username
DB_PASSWORD=your_database_password

Run the migrations:
php artisan migrate --force

Install frontend assets:
npm install

Compile frontend assets:
npm run dev

Start the development server:
php artisan serve

Email Reminders
MAIL_MAILER=smtp
MAIL_HOST=your_mail_host
MAIL_PORT=your_mail_port
MAIL_USERNAME=your_mail_username
MAIL_PASSWORD=your_mail_password
MAIL_ENCRYPTION=your_mail_encryption
MAIL_FROM_ADDRESS=your_from_address
MAIL_FROM_NAME="${APP_NAME}"

Run the Scheduler for reminder emails

php artisan queue:work

Automatic Deletion of Past Appointments
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1

