<?php

namespace App\Mail;

use App\Models\Appointment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReminderEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public string $title;
    public string $daysBefore;



    /**
     * Create a new message instance.
     */
    public function __construct(string $title, string $daysBefore)
    {
        $this->title = $title;
        $this->daysBefore = $daysBefore;
    }

    public function build()
    {
        return $this->view('emails.reminder')
            ->with([
                'title' => $this->title,
                'days_before' => $this->daysBefore,
            ]);
    }

}
