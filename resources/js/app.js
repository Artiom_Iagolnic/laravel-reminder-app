document.addEventListener("DOMContentLoaded", function () {
    try {
        const form = document.getElementById("appointment-form");
        const submitButton = document.getElementById("submit-btn");
        const loadingMessage = document.getElementById("loading-message");

        if (form && submitButton && loadingMessage) {
            form.addEventListener("submit", function () {
                submitButton.setAttribute("disabled", "disabled");
                loadingMessage.style.display = "block";
            });
        }
    } catch (error) {
        console.error("Error:", error);
    }
});
