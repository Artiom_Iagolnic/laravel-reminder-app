@extends('layouts.main')

@section('content')
    <div class="container">
        <h2 class="mt-4">Hi, {{ $user->name }}!</h2>
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Add new appointment
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1 class="modal-title fs-5" id="exampleModalLabel">Add new appointment</h1>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="{{ route('appointments.store') }}" id="appointment-form">
                            @csrf
                            <div class="mb-3">
                                <label for="title" class="form-label">Title</label>
                                <input type="text" class="form-control" id="title" name="title" required
                                    value={{ old('title') }}>
                                @error('title')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="description" class="form-label">Description</label>
                                <textarea name="description" class="form-control" id="description" cols="30" rows="5" required>{{ old('description') }}</textarea>
                                @error('description')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="start-time" class="form-label">Start Time</label>
                                <input type="datetime-local" name="start-time" class="form-control" id="start-time"
                                    required>
                                @error('start-time')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="end-time" class="form-label">End Time</label>
                                <input type="datetime-local" name="end-time" class="form-control" id="end-time" required>
                                @error('end-time')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="reminder" class="form-label">Reminder</label>
                                <select name="reminder" class="form-select" id="reminder" required>
                                    <option value="1">Remind 1 day before</option>
                                    <option value="2">Remind 2 days before</option>
                                    <option value="7">Remind 1 week before</option>
                                </select>
                                @error('reminder')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                    </div>
                    <div class="modal-footer">
                        <div id="loading-message" style="display: none;">Creating appointment...</div>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submit-btn">Add new appointment</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        @if ($appointments->isEmpty())
            <p>No appointments available.</p>
        @else
            <table class="table mt-4">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Description</th>
                        <th scope="col">Start Time</th>
                        <th scope="col">End Time</th>
                        <th scope="col">Reminder</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($appointments as $appointment)
                        <tr>
                            <th scope="row">{{ $loop->index + 1 }}</th>
                            <td>
                                <a href={{ route('appointments.show', $appointment->id) }}
                                    class="text-decoration-none text-primary fw-bold">
                                    {{ $appointment->title }}
                                </a>
                            </td>
                            <td>{{ $appointment->description }}</td>
                            <td>{{ $appointment->start_time }}</td>
                            <td>{{ $appointment->end_time }}</td>
                            <td>{{ $appointment->reminder->days_before }} days before</td>
                            <td>
                                <form method="POST" action="{{ route('appointments.destroy', $appointment->id) }}"
                                    class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-danger btn-block">
                                        <i class="bi bi-trash"></i> Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        @endif

    </div>
@endsection
