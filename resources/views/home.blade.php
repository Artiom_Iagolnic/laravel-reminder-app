@extends('layouts.main')


@section('content')
    <div class="row align-items-center g-lg-5 py-5">
        <div class="col-lg-12 text-center text-lg-start">
            <h1 class="display-4 fw-bold lh-1 text-body-emphasis mb-3">Vertically centered hero sign-up form
            </h1>
            <p class="col-lg-10 fs-4">Below is an example form built entirely with Bootstrap’s form controls.
                Each required form group has a validation state that can be triggered by attempting to submit
                the form without completing it.</p>
        </div>
    </div>
@endsection
