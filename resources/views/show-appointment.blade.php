@extends('layouts.main')

@section('content')
    <form method="post" action="{{ route('appointments.update', $appointment->id) }}">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="title" class="form-label">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $appointment->title }}" required>
            @error('title')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Description</label>
            <textarea name="description" class="form-control" id="description" cols="30" rows="5" required>{{ $appointment->description }}</textarea>
            @error('description')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="start-time" class="form-label">Start Time</label>
            <input type="datetime-local" name="start-time" class="form-control" id="start-time"
                value="{{ $appointment->start_time }}">
            @error('start-time')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="end-time" class="form-label">End Time</label>
            <input type="datetime-local" name="end-time" class="form-control" id="end-time"
                value="{{ $appointment->end_time }}">
            @error('end-time')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="reminder" class="form-label">Reminder</label>
            <select name="reminder" class="form-select" id="reminder" required>
                <option value="1" {{ $appointment->reminder->days_before == 1 ? 'selected' : '' }}>Remind 1 day before
                </option>
                <option value="2" {{ $appointment->reminder->days_before == 2 ? 'selected' : '' }}>Remind 2 days
                    before</option>
                <option value="7" {{ $appointment->reminder->days_before == 7 ? 'selected' : '' }}>Remind 1 week
                    before</option>
            </select>
            @error('reminder')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit Appointment</button>
    </form>
@endsection
