<?php

namespace App\Models;

use App\Models\Appointment;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Reminder extends Model
{
    use HasFactory;

    protected $fillable = [
        'appointment_id',
        'days_before',
        'job_id'
    ];

    public function appointments()
    {
        return $this->belongsTo(Appointment::class);
    }
}
