<p>Hi {{ $name }}</p>
<p>Your new appointment titled "{{ $title }}" has been successfully created.</p>
<p>Your will be reminder at "{{ $dateTime }}" about {{ $title }}.</p>
<p>Thank you for using our service!</p>
