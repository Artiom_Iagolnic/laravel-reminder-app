<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\View\View;
use App\Mail\ReminderEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\RedirectResponse;

class RegisterController extends Controller
{
    public function index(): View
    {
        return view('registration');
    }

    public function register(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|string|min:3|max:55',
            'email' => 'required|string|unique:users|max:255',
            'password' => 'required|string|min:8|confirmed',
            'terms' => 'required|accepted'
        ]);

        User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);
       
        return redirect()->route('home')->with('success', 'Registration successful! , please login');
    }
}
